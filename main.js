const btn = document.querySelector('.talk');
const btnStop = document.querySelector('.stop');
const content = document.querySelector('.content');
const list = document.querySelector('.collection');

const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
const recognition = new SpeechRecognition();

const speech = new SpeechSynthesisUtterance();


let arrHello = ['Hi, how are you?','Hello, sir.','Greetings, how can I help?'];
let arrWeather = ['the weather is pretty good!','Seems to be getting a bit chilly','Looks like it is about to pour'];
let defaultMessage = "I'm sorry, I dont quiet get that.";

btn.addEventListener('click', () => {
    btn.textContent = 'Listening ...';
    recognition.start();
});

recognition.onstart = () => {
    console.log('Recognistion has been activated');
}

recognition.onresult = (e) => {
    console.log('Processng Voice');
    btn.textContent = 'Processing ...';
    const resultIndex = e.resultIndex;
    const transcript = e.results[resultIndex][0].transcript;
    content.textContent = transcript;

    addLst(transcript,'You');

    let response = defaultMessage;

    if(transcript.includes('hello') || transcript.includes('hi')){
        response = arrHello[Math.floor(Math.random() * arrHello.length)];
    }
    if(transcript.includes('weather')){
        response = arrWeather[Math.floor(Math.random() * arrWeather.length)];
    }
    if(transcript.includes('nice')){
        response = "Shut the fuck up you Pussy!";
    }
    if(transcript.includes('enable') && transcript.includes('dark')){
        darkMode();
        response = 'Setting Dark Mode';
    }
    if(transcript.includes('enable') && transcript.includes('light')){
        // darkMode();
        response = 'Git the fuck out of here.';
    }
    
    speechSynth(response);
} 

const speechSynth = (transcript) => {
    addLst(transcript,'Bot');
    speech.text = transcript;
    speech.volume = 1;
    speech.rate = 1;
    speech.pitch = 9;

    window.speechSynthesis.speak(speech);
    btn.textContent = 'Start Listening';
}

function addLst(message,from){
    var node = document.createElement("LI");                 // Create a <li> node
    var textnode = document.createTextNode(from + ' : ' + message.toUpperCase());  // Create a text node
    node.classList.add('collection-item');
    if(from == 'You'){
        node.classList.add('teal');
        node.classList.add('lighten-2');
    }
    node.appendChild(textnode);                              // Append the text to <li>
    list.appendChild(node); 
}

function darkMode(){
    const body = document.querySelector('body');
    body.classList.add('black')
}